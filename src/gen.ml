open Llvm

exception Error of string

let context = global_context ()

let modul = create_module context "jit"

let builder = builder context

let named_values: (string, llvalue) Hashtbl.t = Hashtbl.create 10

let double_type = double_type context

let create_entry_block_alloca func var_name =
    let builder = builder_at context (instr_begin (entry_block func)) in
    build_alloca double_type var_name builder

let create_argument_allocas func proto =
    let args =
        match proto with
        | Ast.Prototype (_, args) | Ast.BinOpPrototype (_, args, _) -> args
    in
    Array.iteri (fun i arg ->
        let var_name = args.(i) in
        let alloca = create_entry_block_alloca func var_name in
        ignore (build_store arg alloca builder);
        Hashtbl.add named_values var_name alloca;
    ) (params func)

let rec codegen_expr = function
    | Ast.Number num -> const_float double_type num
    | Ast.Variable name ->
        let var =
            try
                Hashtbl.find named_values name
            with
            | Not_found -> raise (Error "unknown variable name")
        in
        build_load var name builder
    | Ast.Binary (op, left, right) ->
        if op = '=' then
            let name =
                match left with
                | Ast.Variable name -> name
                | _ -> raise (Stream.Error "destination of '=' must be a variable")
            in
            let value = codegen_expr right in
            let variable =
                try
                    Hashtbl.find named_values name
                with
                | Not_found -> raise (Stream.Error "Unknown variable name")
            in
            ignore (build_store value variable builder);
            value
        else (
            let left_value = codegen_expr left in
            let right_value = codegen_expr right in
            match op with
            | '+' -> build_fadd left_value right_value "addtmp" builder
            | '-' -> build_fsub left_value right_value "subtmp" builder
            | '*' -> build_fmul left_value right_value "multmp" builder
            | '<' ->
                let bool = build_fcmp Fcmp.Ult left_value right_value "cmptmp" builder in
                build_uitofp bool double_type "booltmp" builder
            | _ ->
                let callee = "binary" ^ (String.make 1 op) in
                let callee =
                    match lookup_function callee modul with
                    | Some callee -> callee
                    | None -> raise (Error "binary operator not found")
                in
                build_call callee [|left_value; right_value|] "binop" builder
        )
    | Ast.Call (callee, args) ->
        let callee =
            match lookup_function callee modul with
            | Some callee -> callee
            | None -> raise (Error "unknown function referenced")
        in
        let params = params callee in
        if Array.length params = Array.length args then
            ()
        else
            raise (Error "incorrect # arguments passed");
        let args = Array.map codegen_expr args in
        build_call callee args "calltmp" builder
    | Ast.If (condition, true_expr, false_expr) ->
        let condition = codegen_expr condition in
        let zero = const_float double_type 0.0 in
        let cond_val = build_fcmp Fcmp.One condition zero "ifcond" builder in
        let start_block = insertion_block builder in
        let func = block_parent start_block in
        let then_block = append_block context "then" func in
        position_at_end then_block builder;
        let then_val = codegen_expr true_expr in
        let new_then_block = insertion_block builder in
        let else_block = append_block context "else" func in
        position_at_end else_block builder;
        let else_val = codegen_expr false_expr in
        let new_else_block = insertion_block builder in
        let merge_block = append_block context "ifcont" func in
        position_at_end merge_block builder;
        let incoming = [(then_val, new_then_block); (else_val, new_else_block)] in
        let phi = build_phi incoming "iftmp" builder in
        position_at_end start_block builder;
        ignore (build_cond_br cond_val then_block else_block builder);
        position_at_end new_then_block builder;
        ignore (build_br merge_block builder);
        position_at_end new_else_block builder;
        ignore (build_br merge_block builder);
        position_at_end merge_block builder;
        phi
    | Ast.For (var_name, start, end_var, step, body) ->
        let func = block_parent (insertion_block builder) in
        let alloca = create_entry_block_alloca func var_name in
        let start_val = codegen_expr start in
        ignore (build_store start_val alloca builder);
        let loop_block = append_block context "loop" func in
        ignore (build_br loop_block builder);
        position_at_end loop_block builder;
        let old_val =
            try
                Some (Hashtbl.find named_values var_name)
            with Not_found -> None
        in
        Hashtbl.add named_values var_name alloca;
        ignore (codegen_expr body);
        let step_val =
            match step with
            | Some step -> codegen_expr step
            | None -> const_float double_type 1.0
        in
        let end_condition = codegen_expr end_var in
        let current_val = build_load alloca var_name builder in
        let next_val = build_fadd current_val step_val "nextvar" builder in
        ignore (build_store next_val alloca builder);
        let zero = const_float double_type 0.0 in
        let end_condition = build_fcmp Fcmp.One end_condition zero "loopcond" builder in
        let after_block = append_block context "afterloop" func in
        ignore (build_cond_br end_condition loop_block after_block builder);
        position_at_end after_block builder;
        (match old_val with
        | Some old_val -> Hashtbl.add named_values var_name old_val
        | None -> ()
        );
        const_null double_type
    | Ast.Unary (op, operand) ->
        let operand = codegen_expr operand in
        let callee = "unary" ^ (String.make 1 op) in
        let callee =
            match lookup_function callee modul with
            | Some callee -> callee
            | None -> raise (Stream.Error "unknown unary operator")
        in
        build_call callee [|operand|] "unop" builder
    | Ast.Var (var_names, body) ->
        let old_bindings = ref [] in
        let func = block_parent (insertion_block builder) in
        Array.iter (fun (var_name, init) ->
            let init_val =
                match init with
                | Some init -> codegen_expr init
                | None -> const_float double_type 0.0
            in
            let alloca = create_entry_block_alloca func var_name in
            ignore (build_store init_val alloca builder);
            try
                let old_value = Hashtbl.find named_values var_name in
                old_bindings := (var_name, old_value) :: !old_bindings
            with Not_found -> ();
            Hashtbl.add named_values var_name alloca
        ) var_names;
        let body_val = codegen_expr body in
        List.iter (fun (var_name, old_value) ->
            Hashtbl.add named_values var_name old_value
        ) !old_bindings;
        body_val

let codegen_proto = function
    | Ast.Prototype (name, args) | Ast.BinOpPrototype (name, args, _) ->
        let doubles = Array.make (Array.length args) double_type in
        let ft = function_type double_type doubles in
        let func =
            match lookup_function name modul with
            | None -> declare_function name ft modul
            | Some func ->
                if Array.length (basic_blocks func) == 0 then
                    ()
                else
                    raise (Error "redefinition of function");

                if Array.length (params func) == Array.length args then
                    ()
                else
                    raise (Error "redefinition of function with different # args");
                func
        in
        Array.iteri (fun i a ->
            let n = args.(i) in
            set_value_name n a;
            Hashtbl.add named_values n a;
        ) (params func);
        func

let codegen_func pass_manager = function
    | Ast.Function (proto, body) ->
        Hashtbl.clear named_values;
        let func = codegen_proto proto in

        (match proto with
        | Ast.BinOpPrototype (name, args, precedence) ->
            let op = name.[String.length name - 1] in
            Hashtbl.add Parser.binop_precedence op precedence;
        | _ -> ()
        );

        let basic_block = append_block context "entry" func in
        position_at_end basic_block builder;
        try
            create_argument_allocas func proto;
            let return_value = codegen_expr body in
            let _ = build_ret return_value builder in
            Llvm_analysis.assert_valid_function func;
            ignore (PassManager.run_function func pass_manager);
            func
        with error ->
            delete_function func;
            raise error
